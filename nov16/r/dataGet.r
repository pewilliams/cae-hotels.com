############
#HOTELS.COM
#CAE on Competitive Networks


dyn.load(Sys.getenv('JVM_DYLIB'))
library(RJDBC)
library(data.table)
library(lme4)
library(ggplot2)

#grab the npower data 
jar <- "/Applications/RazorSQL.app/Contents/Java/drivers/jtds/jtds12.jar"
drv <- JDBC("net.sourceforge.jtds.jdbc.Driver", jar, "'")
conn <- dbConnect(drv, Sys.getenv('JDBC_AUDDATA'), Sys.getenv('JDBC_USER'), Sys.getenv('JDBC_PWD')) 
aquery <- "select * from dbo.NPower where AudienceSegmentationCode = 'hotels.com'"
res <- dbSendQuery(conn, aquery) #send query 
adat <- fetch(res, n = -1)
dbClearResult(res) #clear result set to manage memory
dbDisconnect(conn) #close database connection



# ggplot(adat, aes(x=log1p(AAProj))) + geom_density() + facet_wrap(~Network)



#grab the history and future data to join against
hdat <- readRDS("/Volumes/r/cae2/input/AudienceHistoryDatasets.rds")
fdat <- readRDS("/Volumes/r/cae2/input/FutureDataset.rds")

#grab daypart ref data
jar <- "/Applications/RazorSQL.app/Contents/Java/drivers/jtds/jtds12.jar"
drv <- JDBC("net.sourceforge.jtds.jdbc.Driver", jar, "'")
conn <- dbConnect(drv, Sys.getenv('JDBC_REACH'), Sys.getenv('JDBC_USER'), Sys.getenv('JDBC_PWD')) 
dquery <- "select 

c.DaypartName,
c.halfhour,
count(*) n
from 
CostPerHalfHour c

where c.DaypartName <> 'Prime Access'

group by 
c.DaypartName,
c.halfhour

order by 
c.halfhour"

res <- dbSendQuery(conn, dquery) #send query 
ddat <- fetch(res, n = -1)
dbClearResult(res) #clear result set to manage memory
dbDisconnect(conn) #close database connection
ddat <- data.table(ddat)


#grab some reference dates
#grab daypart ref data
jar <- "/Applications/RazorSQL.app/Contents/Java/drivers/jtds/jtds12.jar"
drv <- JDBC("net.sourceforge.jtds.jdbc.Driver", jar, "'")
conn <- dbConnect(drv, Sys.getenv('JDBC_EMERALD'), Sys.getenv('JDBC_USER'), Sys.getenv('JDBC_PWD')) 
dquery <- "select
BroadcastDate, 
BroadcastQuarterNumber,
BroadcastYear
from BroadcastDate
where
BroadcastDate between '2015-06-29' and '2017-06-27'"

res <- dbSendQuery(conn, dquery) #send query 
dateref <- fetch(res, n = -1)
dbClearResult(res) #clear result set to manage memory
dbDisconnect(conn) #close database connection
dateref <- data.table(dateref)
dateref <- transform(dateref, iDate = as.IDate(BroadcastDate, '%Y-%m-%d'))







#clean up and standardize data across the board
adat <- data.table(adat)
hdat <- data.table(hdat[['History']])
fdat <- data.table(fdat)

#target data from npower
#get rid of zero universe stuff
adat <- transform(adat[Universe > 0], iDate = as.IDate(Date, '%Y-%m-%d')) 
adat <- adat[,c('iDate','AudienceSegmentationCode','Network','HalfHourId','AAProj','Universe'),with=F]
adat <- transform(adat, hhkey = paste0(iDate, '-',HalfHourId))

#hdat - join key features to adat - use TBSC since 24 hours/day
hdat <- transform(hdat[NielsenNetwork == 'TBSC'], iDate = as.IDate(Date, '%Y-%m-%d'))
hdat <- hdat[iDate > as.IDate('2015-06-28') & iDate < as.IDate('2016-06-27')]
hdat <- transform(hdat, hhkey = paste0(iDate, '-', HalfHourId))


#join history data features to adat
joinIndex <- match(adat$hhkey , hdat$hhkey)

adat <- transform(adat, 
					cos1 = hdat$cos1[joinIndex],
					cos2 = hdat$cos2[joinIndex],
					cos3 = hdat$cos3[joinIndex],
					cos4 = hdat$cos4[joinIndex],
					cos5 = hdat$cos5[joinIndex],
					cos6 = hdat$cos6[joinIndex],
					sin1 = hdat$sin1[joinIndex],
					sin2 = hdat$sin2[joinIndex],
					sin3 = hdat$sin3[joinIndex],
					sin4 = hdat$sin4[joinIndex],
					sin5 = hdat$sin5[joinIndex],
					sin6 = hdat$sin6[joinIndex],
					cbGD = hdat$cbGD[joinIndex],
					cbCS = hdat$cbCS[joinIndex],
					cbFF = hdat$cbFF[joinIndex],
					cbRepeat = hdat$cbRepeat[joinIndex],
					cbPremiere = hdat$cbPremiere[joinIndex],
					cbLive = hdat$cbLive[joinIndex],
					bcSpecial = hdat$bcSpecial[joinIndex],
					bcRepeat = hdat$bcRepeat[joinIndex],
					bcPremiere = hdat$bcPremiere[joinIndex],
					bcLive = hdat$bcLive[joinIndex],
					bcNews = hdat$bcNews[joinIndex],
					PUTWeekHalfHourCenteredScaled = hdat$PUTWeekHalfHourCenteredScaled[joinIndex],
					Day = format(iDate, '%a'))
					
#add daypart					
adat <- transform(adat, 
					daypart = ddat$DaypartName[match(HalfHourId, ddat$halfhour)], 
					qtr = dateref$BroadcastQuarterNumber[match(iDate, dateref$iDate)], 
					year= format(iDate, '%Y'),
					logUE = log(Universe)) 
					
					
					
#add in demo data
demoData <- read.csv('/Users/pewilliams/Projects/cae-hotels.com/data/demoAudienceHistoryLive.csv',header=T,stringsAsFactors=F)
demoData <- data.table(demoData)
demoData <- transform(demoData, Network = trimws(Network))

#mgsub network names
mgsub <- function(pattern, replacement, x, ...) {
  if (length(pattern)!=length(replacement)) {
    stop("pattern and replacement do not have the same length.")
  }
  result <- x
  for (i in 1:length(pattern)) {
    result <- gsub(pattern[i], replacement[i], result, ...)
  }
  result
}

demoData <- transform(demoData, Network  = mgsub(c('BRAV','ESP2','FOXN','ID','MSNB','SCI','FBN'),c('BRVO','ESPN2','FOXNC','Investigation Discovery','MSNBC','SCIENCE','FOX Business Network'), Network))

#key up date, network, halfhourid
demoData <- transform(demoData, iDate = as.IDate(date, '%m/%d/%y'))
demoData <- transform(demoData, hhkey = paste(iDate, Network, HalfHourId, sep='-'))
adat <- transform(adat, hhkey = paste(iDate, Network, HalfHourId, sep='-'))
adat <- transform(adat, p1849 = demoData$p1849[match(hhkey, demoData$hhkey)], p2554 = demoData$p2554[match(hhkey, demoData$hhkey)])

#save out audience dataset
saveRDS(adat, file = '/Users/pewilliams/Projects/cae-hotels.com/data/audiencehistory.rds')

#ggplot(adat, aes(x=Network, y=log1p(AAProj))) + geom_jitter()


#build out the future dataset
#start with tbsc and then replicate for other networks
setnames(fdat, c('NielsenNetwork','BroadcastQuarterNumber'), c('Network','quarter'))
fdat <- transform(fdat, iDate = as.IDate(Date, '%Y-%d-%m'))
fdat <- transform(fdat, hhkey = paste0(iDate, '-', HalfHourId))
fbase <- fdat[BroadcastQuarter == '2016 Q4']

matchnames <- names(fbase)[names(fbase) %in% names(adat)]

#base future dataset to use for all networks
fbase <- fbase[Network == 'TBSC',
			matchnames, 
			with=F]
fbase$Network <- NULL;


#what needs to be added to future dataset
names(adat)[!(names(adat) %in% names(fbase))]

#logUE
fbase <- transform(fbase,
					logUE = with(adat[qtr == 3], mean(logUE)), #3rd quarter ue
					qtr = 4, 
					year = '2016',
					AAProj = NA, 
					AudienceSegmentationCode = "HOTELS.COM") 
					
#loop through networks and create a base future dataset for each network
futuredataset <- rbindlist(lapply(unique(adat$Network), function(x){
		transform(fbase, Network = x)
		}))
		
futuredataset <- transform(futuredataset, daypart = ddat$DaypartName[match(HalfHourId, ddat$halfhour)])
		
saveRDS(futuredataset, file = '/Users/pewilliams/Projects/cae-hotels.com/data/futuredataset.rds')


#unit rates - to join in later - base on corresponding quarter a year ago

mquery <- "select 
c.DaypartName,
min(c.halfhour) minHH,
max(c.halfhour) maxHH,
rn.networkcode,
c.AverageUnitCostPerDaypart,
rd.broadcastquarter,
count(*) n


from
CostPerHalfHour c

inner join 
refNetwork rn
on 
rn._networksk = c._networksk

inner join 
refDate rd
on 
rd._datesk = c._datesk


where
rd.broadcastquarter = '2015 Q4'

--and rn.networkcode in ('AMC','CMDY','DISC','ESPN2','FUSE','FXX','HGTV','SCIENCE','TLC')

and
c.AverageUnitCostPerDaypart > 0

group by 
c.DaypartName,
rn.networkcode,
c.AverageUnitCostPerDaypart,
rd.broadcastquarter

order by 
rn.networkcode, 
c.DaypartName"

#grab daypart ref data
jar <- "/Applications/RazorSQL.app/Contents/Java/drivers/jtds/jtds12.jar"
drv <- JDBC("net.sourceforge.jtds.jdbc.Driver", jar, "'")
conn <- dbConnect(drv, Sys.getenv('JDBC_REACH'), Sys.getenv('JDBC_USER'), Sys.getenv('JDBC_PWD')) 

res <- dbSendQuery(conn, mquery) #send query 
unitrates <- fetch(res, n = -1)
dbClearResult(res) #clear result set to manage memory
dbDisconnect(conn) #close database connection
unitrates <- data.table(unitrates)

saveRDS(unitrates, file = '/Users/pewilliams/Projects/cae-hotels.com/data/unitrates.rds')




					
				
				
				
				
				































					
					
					
