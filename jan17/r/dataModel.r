#models for hotels.com

library(data.table)
library(lme4)
library(ggplot2)

adat <- readRDS(file = '/Users/pewilliams/Projects/cae-hotels.com/data/audiencehistory.rds')
fdat <- readRDS('/Users/pewilliams/Projects/cae-hotels.com/data/futuredataset.rds')


adat <- transform(adat, 
				Network = factor(Network),
				Day = factor(Day),
				daypart = factor(daypart),
				qtr = factor(qtr),
				yr = ifelse(year == '2016',1,0),
				hhfactor = factor(HalfHourId)
				)

fdat <- transform(fdat, 
				Network = factor(Network, levels = levels(adat$Network)),
				Day = factor(Day, levels = levels(adat$Day)),
				daypart = factor(daypart, levels = levels(adat$daypart)),
				qtr = factor(qtr, levels = levels(adat$qtr)),
				yr = 1,
				hhfactor = factor(HalfHourId, levels=levels(adat$hhfactor))
				)				
				

#build up a model
# mod1 <- lmer(log1p(AAProj) ~ (1|Network) + cos1 + cos2 + cos3 + cos4 + cos5 + sin1 + sin2 + sin3 + sin4 + sin5 + sin6 + PUTWeekHalfHourCenteredScaled + Day, data = adat, REML=F)
# mod2 <- lmer(log1p(AAProj) ~ (PUTWeekHalfHourCenteredScaled|Network) + cos1 + cos2 + cos3 + cos4 + cos5 + sin1 + sin2 + sin3 + sin4 + sin5 + sin6 , data = adat, REML=F)
# mod3 <- lmer(log1p(AAProj) ~ (PUTWeekHalfHourCenteredScaled|Network) + cos1 + cos2 + cos3 + cos4 + cos5 + sin1 + sin2 + sin3 + sin4 + sin5 + sin6 +Day , data = adat, REML=F)
##oos
# jun16 <- adat[iDate >= as.IDate('2016-06-01')]
# train <- adat[iDate < as.IDate('2016-06-01')]
# 
# tmod <- lmer(log1p(AAProj) ~ (PUTWeekHalfHourCenteredScaled|Network) + cos1 + cos2 + cos3 + cos4 + cos5 + sin1 + sin2 + sin3 + sin4 + sin5 + sin6 + Day + daypart + qtr + yr + hhfactor, data = train, REML=T)
# 
# 
# jun16$aaf <- expm1(predict(tmod, newdata=jun16))
# 
# 
# as <- jun16[,list(mf = mean(aaf), act=mean(AAProj)),by=list(Network,daypart)]




modTarget <- lmer(log1p(AAProj) ~ (PUTWeekHalfHourCenteredScaled|Network) + cos1 + cos2 + cos3 + cos4 + cos5 +  sin1 + sin2 + sin3 + sin4 + sin5 + sin6 + Day + daypart + qtr + yr + hhfactor, data = adat, REML=T)

modP2554 <- lmer(log1p(p2554) ~ (PUTWeekHalfHourCenteredScaled|Network) + cos1 + cos2 + cos3 + cos4 + cos5 + cos6 + sin1 + sin2 + sin3 + sin4 + sin5 + sin6 + Day + daypart + qtr + yr + hhfactor, data = adat, REML=T)

modP1849 <- lmer(log1p(p1849) ~ (PUTWeekHalfHourCenteredScaled|Network) + cos1 + cos2 + cos3 + cos4 + cos5 + cos6 + sin1 + sin2 + sin3 + sin4 + sin5 + sin6 + Day + daypart + qtr + yr + hhfactor, data = adat, REML=T)


# AIC(mod1)
# AIC(mod2)
# AIC(mod3)
# AIC(mod4)
# anova(mod1,mod2, mod3, mod4)


fdat$TargetAA <- expm1(predict(modTarget, newdata=fdat)) #forecasts all half hours
fdat$P2554AA <- expm1(predict(modP2554, newdata=fdat)) #forecasts all half hours
fdat$P1849AA <- expm1(predict(modP1849, newdata=fdat)) #forecasts all half hours


#need to truncate forecast dataset to half hours where the network typically airs
hhrange <- adat[,list(n=.N,minHH=min(HalfHourId), maxHH=max(HalfHourId)), by =list(Network)]

od <- lapply(hhrange$Network, function(x) {
	netdat <- fdat[Network == x]
	hhr <- hhrange[Network == x]
	netdat[HalfHourId >= hhr$minHH & HalfHourId <= hhr$maxHH]	
	})
	
od <- rbindlist(od)

od <- fdat[order(iDate, Network, HalfHourId, decreasing = F)] #ouput dataset with forecasts in it

#join in unit rates

mgsub <- function(pattern, replacement, x, ...) {
  if (length(pattern)!=length(replacement)) {
    stop("pattern and replacement do not have the same length.")
  }
  result <- x
  for (i in 1:length(pattern)) {
    result <- gsub(pattern[i], replacement[i], result, ...)
  }
  result
}

ur <- readRDS('/Users/pewilliams/Projects/cae-hotels.com/data/unitrates.rds')


#match up networks
od <- transform(od, Network = as.character(Network))
missnets <- unique(od$Network)[!(unique(od$Network) %in% unique(ur$networkcode))]
od <- transform(od, Network = mgsub(c("Investigation Discovery","SPK"),c("ID","SPIKE"), Network))

#get missing nets
pmiss <- read.csv('/Users/pewilliams/Projects/cae-hotels.com/data/remnantNetPricing.csv',header=T,stringsAsFactors=F) #q415
pmiss <- data.table(pmiss)
pmiss <- pmiss[,list(Rate = weighted.mean(Rate,Count)),by=list(Channel,DaypartName)]
pmiss <- transform(pmiss, nkey = paste0(Channel,'-',DaypartName))

ur <- transform(ur, nkey = paste0(networkcode,'-',DaypartName))

#first join in rates from unitrates datasets, then join in from the remnant network pricing table, all against nkey

od <- transform(od, nkey = paste0(Network, '-',daypart))

od1 <- od[!(Network %in% unique(pmiss$Channel))]
od2 <- od[Network %in% unique(pmiss$Channel)]

od1 <- transform(od1, rate = ur$AverageUnitCostPerDaypart[match(nkey, ur$nkey)])

od2 <- transform(od2, rate = pmiss$Rate[match(nkey, pmiss$nkey)])

od <- rbindlist(list(od1,od2))

#data for extract

extract <- od[,c('hhkey','Day', 'HalfHourId', 'iDate', 'TargetAA', 'P2554AA','P1849AA','AudienceSegmentationCode', 'Network', 'daypart', 'rate'),with=F]

extract <- transform(extract, TargetAA = ifelse(TargetAA < 0, 0, TargetAA))

write.csv(extract, file = '/Users/pewilliams/Projects/cae-hotels.com/data/projectionsAndRatesExtractWithDemos.csv', row.names=F)













