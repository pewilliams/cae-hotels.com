############
#HOTELS.COM POC
#CAE on using LPM Data
#

#depends
dyn.load(Sys.getenv('JVM_DYLIB'))
library(RJDBC)
library(data.table)
library(parallel)


#grab the audience data from the npower files
cfiles <- dir('jan17/data/csv')
cfiles <- cfiles[grepl('.csv',cfiles,fixed=T)] #filter on extension

#read in data
adat <- mclapply(cfiles, function(x){
	rdat <- fread(paste0('jan17/data/csv/',x),header=T,stringsAsFactors=F)
	rdat
},mc.cores=20)

#combine data together - reference columns names across files just in case
adat <- rbindlist(adat,use.names=T, fill=T)

setnames(adat, names(adat), 
	c('covArea','mbreak','demo','date','net','hh','feed','playback','dur','rtg','aa',
		'index','base','wtgMin','pess','avgIntab','scaledIntab','scaledInstall','ue','ueType'))
		
adat$playback <- NULL; #all live+sd | linear w/ vod
adat$feed <- NULL; #live nets are live (e.g. news), others are dual
adat$pess <- NULL; #dont care
adat$scaledIntab <- NULL;
adat$ueType <- NULL; #some are S, some are C - should ask at some point
adat <- adat[dur >0]; #all hhs should be 30 mins
adat$dur <- NULL;
adat$index <- NULL; #unimportant
adat$wtgMin <- NULL; #dont care
adat$base <- NULL; #same as UE
adat$scaledInstall <- NULL; #nope
adat$covArea <- NULL; #all total US

#get rid of commas
commaToNumeric <- function(x) { as.numeric(gsub(',','',x)) }

adat <- transform(adat, ue = commaToNumeric(ue), avgIntab = commaToNumeric(avgIntab),
	date = as.IDate(date, '%m/%d/%y'))

#get rid of composite
adat <- adat[!(mbreak %in% c('Composite','HH Income = $75,000+'))]
#only be concerned with target - not demo

adat$aa <- as.numeric(adat$aa)
#adat 
p1899aa <- adat[demo == 'Persons 18 - 99']

adat <- adat[demo != 'Persons 18 - 99']

saveRDS(adat, file = 'jan17/data/rds/audiences.rds')

#get approximate aa to match to p18-49 cpms


p1899aa <- p1899aa[,.(aa=mean(aa)),by=.(hh,mbreak,demo,net)]
p1899aa <- transform(p1899aa, p1849index = 128.321/237.840)
p1899aa <- transform(p1899aa, p1849aaEst = p1849index * aa)


#unit rates - from ad-intel to get relative network pricing at national level

mquery <- "select 
c.DaypartName,
min(c.halfhour) minHH,
max(c.halfhour) maxHH,
rn.networkcode,
c.AverageUnitCostPerDaypart,
rd.broadcastquarter,
count(*) n


from
CostPerHalfHour c

inner join 
refNetwork rn
on 
rn._networksk = c._networksk

inner join 
refDate rd
on 
rd._datesk = c._datesk

where

rd.broadcastquarter = '2015 Q4'

and 
rn.networkcode in ('DISC','FBN','FOXNC','FXX','HALL','HGTV', 'HIST', 'MSNBC','SCI','TRAV')

and
c.AverageUnitCostPerDaypart > 0

group by 
c.DaypartName,
rn.networkcode,
c.AverageUnitCostPerDaypart,
rd.broadcastquarter

order by 
rn.networkcode, 
c.DaypartName"

#grab daypart ref data
jar <- "/Applications/RazorSQL.app/Contents/Java/drivers/jtds/jtds12.jar"
drv <- JDBC("net.sourceforge.jtds.jdbc.Driver", jar, "'")
conn <- dbConnect(drv, Sys.getenv('JDBC_REACH'), Sys.getenv('JDBC_USER'), Sys.getenv('JDBC_PWD')) 

res <- dbSendQuery(conn, mquery) #send query
 
unitrates <- fetch(res, n = -1)
unitrates <- data.table(unitrates)

# mdat <- fetch(res, n = -1) #daypart ref
# mdat <- data.table(mdat)
# mdat <- mdat[1:6,c('minHH','maxHH','DaypartName'),with=F]

dbClearResult(res) #clear result set to manage memory
dbDisconnect(conn) #close database connection



#cross join half hour against hh ranges
cjHH <- function(a){
	nrecs <- a$maxHH - a$minHH + 1
	udat <- rbindlist(lapply(1:nrecs, function(x) a))
	udat <- transform(udat, hh = seq(a$minHH, a$maxHH, by =1))
	udat$minHH <- NULL;
	udat$maxHH <- NULL;
	udat
}

# a <- unitrates[1]
# cjHH(a)

#iterate over rows and collapse - there has to be a better way
urList <- list(cjHH(unitrates[1]))
for(i in 2:nrow(unitrates)){ urList <- append(urList, list(cjHH(unitrates[i])))}
unitrates <- rbindlist(urList)

saveRDS(unitrates, file = 'jan17/data/rds/ratereference.rds')

#bring in sqad rates
sqad <- fread('jan17/data/rates/sqadP1849CPM.csv', header=T,sep=',')
sqad <- melt.data.table(sqad, id.var=c('SQADMarket','MBMarket'), variable.factor=F)
setnames(sqad, c('variable','value'),c('daypart','cpm')) # these are all p1849 cpms

#harmonize names
unitrates$DaypartName <- gsub('Morning','Early Morning',unitrates$DaypartName)
# mdat$DaypartName <- gsub('Morning','Early Morning',mdat$DaypartName)
sqad <- sqad[daypart != 'Evening News']
unitrates <- unitrates[!(DaypartName == 'Prime' & hh == 27)]

#compute network,daypart price indices - for comparison at national level of relative pricing
netRateRef <- unitrates[,.(rate=mean(AverageUnitCostPerDaypart)),by=.(DaypartName,networkcode)]
dayRateRef <-unitrates[,.(rate=mean(AverageUnitCostPerDaypart)),by=.(DaypartName)] 
netRateRef <- merge(netRateRef,dayRateRef, by = c('DaypartName'), all.x=T)
netRateRef <- transform(netRateRef, rateIndex = rate.x/rate.y) #relative national rate

#clean up p1899aa file to get daypart clean names by half hour daypart
hhRef <- fread('jan17/data/rates/HalfHourRef.csv',header=T,sep=',')

p1899aa <- transform(p1899aa, hh = hhRef$hh[match(hh,hhRef$hhlab)], 
			daypart = hhRef$daylab[match(hh,hhRef$hhlab)])

#need to map in market rate against the p1849aa table
#first get the daypart in there
setnames(sqad, 'MBMarket', 'mbreak')
sqad <- transform(sqad, daypart = gsub('Late Night','Overnight',daypart))
#add daypart name to p1899aa

rs <- transform(p1899aa, cpm = sqad$cpm[match(daypart,sqad$daypart)])

rs <- rs[,.(p1899aa = mean(aa), p1849aaEst = mean(p1849aaEst), cpm = mean(cpm)),by=.(mbreak,net,demo, daypart)]

#add price index
nr <- netRateRef[,c('DaypartName','networkcode','rateIndex'),with=F]
setnames(nr, c('DaypartName','networkcode'), c('daypart','net'))
#clean up net names
mgsub <- function(pattern, replacement, x, ...) {
  if (length(pattern)!=length(replacement)) {
    stop("pattern and replacement do not have the same length.")
  }
  result <- x
  for (i in 1:length(pattern)) {
    result <- gsub(pattern[i], replacement[i], result, ...)
  }
  result
}

rs <- rs[net != 'SCIENCE']


rs <- transform(rs, net = gsub(
	"FOX Business Network", "FBN", net)
	)


rs <- merge(rs, nr, all.x=T, by=c('daypart','net'))














































#OLD STUFF
















#grab the history and future data to join against
hdat <- readRDS("/Volumes/r/cae2/input/AudienceHistoryDatasets.rds")
fdat <- readRDS("/Volumes/r/cae2/input/FutureDataset.rds")


#grab some reference dates
#grab daypart ref data
jar <- "/Applications/RazorSQL.app/Contents/Java/drivers/jtds/jtds12.jar"
drv <- JDBC("net.sourceforge.jtds.jdbc.Driver", jar, "'")
conn <- dbConnect(drv, Sys.getenv('JDBC_EMERALD'), Sys.getenv('JDBC_USER'), Sys.getenv('JDBC_PWD')) 
dquery <- "select
BroadcastDate, 
BroadcastQuarterNumber,
BroadcastYear
from BroadcastDate
where
BroadcastDate between '2015-06-29' and '2017-06-27'"

res <- dbSendQuery(conn, dquery) #send query 
dateref <- fetch(res, n = -1)
dbClearResult(res) #clear result set to manage memory
dbDisconnect(conn) #close database connection
dateref <- data.table(dateref)
dateref <- transform(dateref, iDate = as.IDate(BroadcastDate, '%Y-%m-%d'))







#clean up and standardize data across the board
adat <- data.table(adat)
hdat <- data.table(hdat[['History']])
fdat <- data.table(fdat)

#target data from npower
#get rid of zero universe stuff
adat <- transform(adat[Universe > 0], iDate = as.IDate(Date, '%Y-%m-%d')) 
adat <- adat[,c('iDate','AudienceSegmentationCode','Network','HalfHourId','AAProj','Universe'),with=F]
adat <- transform(adat, hhkey = paste0(iDate, '-',HalfHourId))

#hdat - join key features to adat - use TBSC since 24 hours/day
hdat <- transform(hdat[NielsenNetwork == 'TBSC'], iDate = as.IDate(Date, '%Y-%m-%d'))
hdat <- hdat[iDate > as.IDate('2015-06-28') & iDate < as.IDate('2016-06-27')]
hdat <- transform(hdat, hhkey = paste0(iDate, '-', HalfHourId))


#join history data features to adat
joinIndex <- match(adat$hhkey , hdat$hhkey)

adat <- transform(adat, 
					cos1 = hdat$cos1[joinIndex],
					cos2 = hdat$cos2[joinIndex],
					cos3 = hdat$cos3[joinIndex],
					cos4 = hdat$cos4[joinIndex],
					cos5 = hdat$cos5[joinIndex],
					cos6 = hdat$cos6[joinIndex],
					sin1 = hdat$sin1[joinIndex],
					sin2 = hdat$sin2[joinIndex],
					sin3 = hdat$sin3[joinIndex],
					sin4 = hdat$sin4[joinIndex],
					sin5 = hdat$sin5[joinIndex],
					sin6 = hdat$sin6[joinIndex],
					cbGD = hdat$cbGD[joinIndex],
					cbCS = hdat$cbCS[joinIndex],
					cbFF = hdat$cbFF[joinIndex],
					cbRepeat = hdat$cbRepeat[joinIndex],
					cbPremiere = hdat$cbPremiere[joinIndex],
					cbLive = hdat$cbLive[joinIndex],
					bcSpecial = hdat$bcSpecial[joinIndex],
					bcRepeat = hdat$bcRepeat[joinIndex],
					bcPremiere = hdat$bcPremiere[joinIndex],
					bcLive = hdat$bcLive[joinIndex],
					bcNews = hdat$bcNews[joinIndex],
					PUTWeekHalfHourCenteredScaled = hdat$PUTWeekHalfHourCenteredScaled[joinIndex],
					Day = format(iDate, '%a'))
					
#add daypart					
adat <- transform(adat, 
					daypart = ddat$DaypartName[match(HalfHourId, ddat$halfhour)], 
					qtr = dateref$BroadcastQuarterNumber[match(iDate, dateref$iDate)], 
					year= format(iDate, '%Y'),
					logUE = log(Universe)) 
					
					
					
#add in demo data
demoData <- read.csv('/Users/pewilliams/Projects/cae-hotels.com/data/demoAudienceHistoryLive.csv',header=T,stringsAsFactors=F)
demoData <- data.table(demoData)
demoData <- transform(demoData, Network = trimws(Network))

#mgsub network names
mgsub <- function(pattern, replacement, x, ...) {
  if (length(pattern)!=length(replacement)) {
    stop("pattern and replacement do not have the same length.")
  }
  result <- x
  for (i in 1:length(pattern)) {
    result <- gsub(pattern[i], replacement[i], result, ...)
  }
  result
}

demoData <- transform(demoData, Network  = mgsub(c('BRAV','ESP2','FOXN','ID','MSNB','SCI','FBN'),c('BRVO','ESPN2','FOXNC','Investigation Discovery','MSNBC','SCIENCE','FOX Business Network'), Network))

#key up date, network, halfhourid
demoData <- transform(demoData, iDate = as.IDate(date, '%m/%d/%y'))
demoData <- transform(demoData, hhkey = paste(iDate, Network, HalfHourId, sep='-'))
adat <- transform(adat, hhkey = paste(iDate, Network, HalfHourId, sep='-'))
adat <- transform(adat, p1849 = demoData$p1849[match(hhkey, demoData$hhkey)], p2554 = demoData$p2554[match(hhkey, demoData$hhkey)])

#save out audience dataset
saveRDS(adat, file = '/Users/pewilliams/Projects/cae-hotels.com/data/audiencehistory.rds')

#ggplot(adat, aes(x=Network, y=log1p(AAProj))) + geom_jitter()


#build out the future dataset
#start with tbsc and then replicate for other networks
setnames(fdat, c('NielsenNetwork','BroadcastQuarterNumber'), c('Network','quarter'))
fdat <- transform(fdat, iDate = as.IDate(Date, '%Y-%d-%m'))
fdat <- transform(fdat, hhkey = paste0(iDate, '-', HalfHourId))
fbase <- fdat[BroadcastQuarter == '2016 Q4']

matchnames <- names(fbase)[names(fbase) %in% names(adat)]

#base future dataset to use for all networks
fbase <- fbase[Network == 'TBSC',
			matchnames, 
			with=F]
fbase$Network <- NULL;


#what needs to be added to future dataset
names(adat)[!(names(adat) %in% names(fbase))]

#logUE
fbase <- transform(fbase,
					logUE = with(adat[qtr == 3], mean(logUE)), #3rd quarter ue
					qtr = 4, 
					year = '2016',
					AAProj = NA, 
					AudienceSegmentationCode = "HOTELS.COM") 
					
#loop through networks and create a base future dataset for each network
futuredataset <- rbindlist(lapply(unique(adat$Network), function(x){
		transform(fbase, Network = x)
		}))
		
futuredataset <- transform(futuredataset, daypart = ddat$DaypartName[match(HalfHourId, ddat$halfhour)])
		
saveRDS(futuredataset, file = '/Users/pewilliams/Projects/cae-hotels.com/data/futuredataset.rds')




saveRDS(unitrates, file = '/Users/pewilliams/Projects/cae-hotels.com/data/unitrates.rds')




					
				
				
				
				
				































					
					
					
